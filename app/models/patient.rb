class Patient < ActiveRecord::Base
	has_many :appointments

  	has_many :specialists, through: :appointments
  
  	belongs_to :insurance
end
