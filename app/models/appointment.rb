class Appointment < ActiveRecord::Base

	belongs_to :patient

	belongs_to :specialist

	validates :fee, numericality: { only_decimal: true, greater_than_or_equal_to: 0 }
	
end
